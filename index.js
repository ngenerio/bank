require('babel-register');

var path = require('path');
var dotenv = require('dotenv');

var envToLoad = (process.env.NODE_ENV === 'development' ? '.dev' : '');
dotenv.load({ path: path.join(__dirname, '.env' + envToLoad )});

require('./src/db');
require('./src/server');

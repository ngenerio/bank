var express = require('express');
var passport = require('passport');
var _ = require('lodash');
var utils = require('../utils');
var logger = require('../logger');

var webRouter = express.Router();

module.exports = function (db) {
  webRouter.get('/login', function (req, res) {
    var flash = req.flash('error');
    if (flash.length) {
      res.locals.flash = {
        type: 'error',
        message: flash[0]
      };
    }

    res.render('login', {
      title: 'Banking Homepage - Log in'
    })
  });

  webRouter.get('/admin', function (req, res, next) {
    if (!req.session.isAuthenticated || !req.session.user) {
        return res.redirect('/admin/login');
      }

    db.Transaction
    .findAll({ limit: 10, include: [ db.User ], order: [['createdAt', 'DESC']] })
    .then((transactions) => {
      logger.info(JSON.stringify(transactions));

      return db.User
      .findAll()
      .then((users) => {
        logger.info(JSON.stringify(users));
        return res.render('admin/index', {
          users: users,
          transactions: transactions
        });
      });
    })
    .catch(function (err) {
      logger.error(err, 'Error occured at /admin');
      next(err);
    });
  });

  webRouter.get('/admin/transaction/:id', function (req, res, next) {
    if (!req.session.isAuthenticated) {
      return res.redirect('/admin/login');
    }

    db.Transaction
    .findById(req.params.id, { include: [ db.User ] })
    .then(function (trx) {
      if (!trx) {
        req.flash('error', 'Could not find that transaction');
        return res.redirect('/admin/error');
      }

      db.Account
      .findById(trx.accountId)
      .then(function (acc) {
        db.Customer.findById(acc.customerId)
        .then(function (customer) {
          logger.info(JSON.stringify(customer));
          res.render('admin/transaction', {
            transaction: trx,
            account: acc,
            customer: customer,
            user: trx.user
          });
        })
        .catch((err) => next(err));

      }).catch((err) => next(err));
    }).catch((err) => next(err));
  });

  webRouter.get('/admin/login', function (req, res) {
    var flash = req.flash('error');
    if (flash.length) {
      res.locals.flash = {
        type: 'error',
        message: flash[0]
      };
    }

    res.render('admin/login', {
      title: 'Admin Log in'
    });
  });

  webRouter.get('/admin/logout', function (req, res) {
    if (req.session.isAuthenticated) {
      req.session.isAuthenticated = false;
      req.session.user = {};
    }

    return res.redirect('/admin/login');
  });

  webRouter.post('/admin/login', function (req, res, next) {
    var  username = req.body.username;
    var password = req.body.password;

    if (username === process.env.ADMIN_USER && password === process.env.ADMIN_PASSWORD) {
      req.session.isAuthenticated = true;
      req.session.user = { isAdmin: true, username: username, password: password };
      return res.redirect('/admin');
    }

    req.flash('error', 'Invalid username or password!');
    res.redirect('/admin/login');
  });

  webRouter.get('/admin/add_user', function (req, res) {
    if (req.session.isAuthenticated && req.session.user && req.session.user.isAdmin) {
      var flash = req.flash('error');
      var flashInfo = req.flash('info');

      if (flash.length || flashInfo.length) {
        res.locals.flash = {
          type: flash.length ? 'danger' : 'primary',
          message: flash.length ? flash[0] : flashInfo[0]
        };
      }
      return res.render('admin/user', {
        title: 'New User - EBank'
      });
    }

    return res.redirect('/admin/login');
  });

  webRouter.get('/search', function (req, res, next) {
    var { searchTerm, searchBy } = req.query;
    var searchPromise;

    if (searchBy === 'name') {
      searchPromise = db.Customer.findAll({ where:
        {
          $or: [
            { firstname: { $like: `%${searchTerm}%`} },
            { lastname:  { $like: `%${searchTerm}%`} }
          ]
        },
        include: [ db.Account ]
      });
    }

    if (searchBy === 'group') {
      searchPromise = db.Customer.findAll({ where: { groupName: { $like: `%${searchTerm}%`} }, include: [ db.Account ] })
    }

    if (searchBy === 'number') {
      searchPromise = db.Account.findAll({ where: { number: { $like: `%${searchTerm}%` } } });
    }


    if (searchPromise) {

      if (searchBy === 'name' || searchBy === 'group') {
        var accountArray = [];

        searchPromise.then(function (customers) {
          logger.info(JSON.stringify(customers));
          customers.forEach(function (customer) {
            if (customer.account) {
              var accountToDisplay = customer.account;
              accountToDisplay.customer = customer;
              accountArray.push(accountToDisplay);
            } else {
              accountArray.push({});
            }
          });

          logger.info(accountArray);
          res.render('search', {
            accounts: accountArray,
            title: `Search - (${searchTerm})`,
            searchTerm: searchTerm
          });
        }).catch((err) => {
          logger.error(err);
          return next(err);
        });
      }

      if (searchBy === 'number') {
        searchPromise.then(function (accounts) {
          var customerPromise = [];
          var accountsArray = [];

          accounts.forEach(function (account) {
            (function (acc) {
              customerPromise.push(db.Customer.findById(acc.id));
            })(account);
          });

          Promise.all(customerPromise)
          .then(function (returnables) {
            logger.info(returnables);

            for (var i = 0, l = returnables.length; i < l; i++) {
              var someAccount = accounts[i];
              someAccount.customer= returnables[i];
              accountsArray.push(someAccount);
            }

            res.render('search', {
              accounts: accountsArray,
              title: `Search - (${searchTerm})`,
              searchTerm: searchTerm
            });
          });

        }).catch((err) => {
          logger.error(err);
          next(err);
        });
      }
    }
  });

  webRouter.get('/account/deposit/:id', function (req, res, next) {
    var { id } = req.params;

    db.Account
    .findById(id)
    .then(function (account) {
      if (account) {
        db.Customer
        .findById(account.customerId)
        .then(function (customer) {
          res.render('deposit', {
            customer: customer,
            account: account,
            title: `Deposit Form - ${account.number}`
          });
        });
      }
    }).catch((err) => next(err));
  });

  webRouter.get('/account/edit/:id', function (req, res, next) {
    var { id } = req.params;

    db.Account
    .findById(id)
    .then(function (account) {
      if (account) {
        db.Customer
        .findById(account.customerId)
        .then(function (customer) {
          res.render('edit', {
            customer: customer,
            account: account,
            title: 'Edit customer information'
          });
        })
        .catch(next)
      }
      else {
        res.locals.flash = {
          type: 'error',
          message: 'Could not find account'
        }
      }
    })
    .catch(next);
  });

  webRouter.post('/account/edit/:id', function (req, res, next) {
    var { id } = req.params;

    db.Customer
    .update(req.body, { where: { id} })
    .then(function (updatedUser) {
      req.flash('info', 'Customer has been updated');
      res.redirect('/customer/new');
    })
    .catch(next);
  });

  webRouter.get('/account/withdraw/:id', function (req, res, next) {
    var { id } = req.params;

    db.Account
    .findById(id)
    .then(function (account) {
      if (account) {
        db.Customer
        .findById(account.customerId)
        .then(function (customer) {
          res.render('withdrawal', {
            customer: customer,
            account: account,
            title: `Withdrawal Form - ${account.number}`
          });
        });
      }
    }).catch((err) => next(err));
  });

  webRouter.post('/admin/add_user', function (req, res, next) {
    if (req.session.isAuthenticated && req.session.user && req.session.user.isAdmin) {
      var { name, username, telephone, email, title = 'Mr.', position = 'Other', gender = 'male', password = '' } = req.body;

      if (password === '') {
        req.flash('error', 'Provide a password for the new user');
        return res.redirect('/admin/add_user');
      }

      if (!email) {
        req.flash('error', 'Provide a password for the new user');
        return res.redirect('/admin/add_user');
      }

      logger.debug(req.body);
      return db
        .User
        .findOne({ where: { email } })
        .then(function (user) {
          if (user) {
            req.flash('error', 'Email already available');
            return res.redirect('/admin/add_user');
          }

          utils.encryptPassword(password, function (err, hash) {
            if (err) return next(err);

            db.User
            .create({
              name, username, telephone, email, password, title, position, gender, password: hash
            })
            .then(function (user) {
              console.log(user);
              req.flash('info', 'User has been created');
              res.redirect('/admin');

              utils.sendMessage(telephone, `Hello ${title} ${name}, you have just been given access to the web portal and app. Your email: ${email} and password: ${password}`, function (err, res) {
                if (err) return logger.error(err);
                logger.info(res);
              });

            })
          });
        })
        .catch((err) => {
          logger.error('Db error: ', err);
        });
    }

    return res.redirect('/admin/login');
  });

  webRouter.get('/admin/user/:id', function (req, res, next) {
    console.log(req.params.id);

    if (req.session.isAuthenticated && req.session.user && req.session.user.isAdmin) {
      return db.User
      .findOne({ where: { id: req.params.id } })
      .then(function (user) {
        if (!user) {
          req.flash('error', 'User not found!');
          return res.redirect('/admin/add_user');
        }

        var flashErrorMessage = req.flash('error');

        if (flashErrorMessage.length) {
          res.locals.flash = {
            type: 'error',
            message: flashErrorMessage[0]
          };
        }

        res.render('admin/user_details', {
          title: `User details: ${user.name}`,
          user: user
        });
      })
      .catch(function (err) {
        next(err);
      });
    }

    return res.redirect('/admin/login');
  });

  webRouter.post('/admin/user/:id', function (req, res, next) {
    if (req.session.isAuthenticated && req.session.user && req.session.user.isAdmin) {
      var { name, username, telephone, email, title = 'Mr.', position = 'Other', gender = 'male' } = req.body;

      logger.info(name, email);
      if (!name || !email) {
        req.flash('error', 'Supply all the user details');
        return res.redirect(`/admin/user/:${req.params.id}`);
      }

      return db.User
      .update(
        { name, username, telephone, email, title, position, gender },
        { where: { id: req.params.id } }
      )
      .then(function () {
        return res.redirect('/admin');
      })
      .catch(function (err) { next(err); })
    }

    return res.redirect('/admin/login');
  });

  webRouter.post('/user/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
      if (err) return next(err);

      if (!user) {
        req.flash('error', info.message);
        return res.redirect('/login');
      }
      user
      .update({ state: 'active' })
      .then((user) => {
        req.logIn(user, function (err) {
          if (err) return next(err);

          return res.redirect('/');
        });
      })
      .catch((err) => {
        return (err);
      });

    })(req, res, next);
  });

  webRouter.get('/', function (req, res) {
    if (req.user) {
      logger.debug(req.user);
      return res.render('index', {
        title: 'Welcome - Homepage'
      });
    }

    return res.redirect('/login');
  });

  webRouter.get('/logout', function (req, res, next) {
    if (req.user) {
      return req.user
      .update({ state: 'inactive' })
      .then(() => {
        req.logout();
        res.redirect('/login');
      })
      .catch((err) => {
        next(err);
      });
    }

    req.logout();
    res.redirect('/');
  });

  webRouter.get('/customer/new', function (req, res) {
    if (req.user) {
      var flashError = req.flash('error');
      var flashInfo = req.flash('info');

      if (flashInfo.length || flashError.length) {
        res.locals.flash = {
          type: flashError.length ? 'danger' : 'info',
          message: flashError[0] || flashInfo[0]
        };
      }

      return res.render('new_customer', {
        title: 'Add Customer'
      });
    }

    return res.redirect('/');
  });

  webRouter.post('/customer/new', function (req, res, next) {
    if (!req.user) {
      return res.redirect('/');
    }

    var { accountType } = req.body;
    _.unset(req.body, 'accountType');

    for (var prop in req.body) {
      if (req.body.hasOwnProperty(prop) && req.body[prop] === 'on') {
        req.body[prop] = true;
      }
    }

    db.Customer
    .create(req.body)
    .then((newCustomer) => {
      return db.Account
      .create({ type: accountType, balance: 0.00, customerId: newCustomer.id })
      .then(function (account) {
        req.flash('info', newCustomer.id);
        logger.info(account.number);
        logger.info('some thing crazy', account.id);
        return res.redirect(`/customer/welcome`);
      })
      .catch(() => {
        return next(err);
      })
    })
    .catch((err) => {
      return next(err);
    });
  });

  webRouter.get('/customer/welcome', function (req, res, next) {
    var id = req.flash('info');

    if (id.length) {
      id = id[0];

      db.Customer
      .findById(id, { include: [ db.Account ] })
      .then(function (customer) {

        db.Account.findOne({ where: { customerId: customer.id }})
        .then(function (acc) {
          customer.account = acc;
          res.render('welcome', {
            title: `${customer.title} ${customer.firstname}`,
            customer: customer
          });
        })
        .catch((err) => next(err));
      })
      .catch(function (err) {
        next(err);
      });
    }
  });

  webRouter.get('/deposit', function (req, res) {
    if (!req.user) return res.redirect('/');

    var flashErrorMessage = req.flash('error');
    var flashInfoMessage = req.flash('info');

    if (flashErrorMessage.length || flashInfoMessage.length) {
      res.locals.flash = {
        type: flashInfoMessage.length ? 'info' : 'danger',
        message: flashInfoMessage.length ? flashInfoMessage[0] : flashErrorMessage[0]
      };
    }

    res.render('deposit', {
      title: 'New Deposit'
    });
  });

  webRouter.post('/deposit', function (req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var { amount, accountName, accountNumber, issuer } = req.body;
    var { user: { id: userId } } = req;

    db.Account
    .find({ where: { number: accountNumber } })
    .then(function (account) {

      if (!account) {
        req.flash('error', 'No account found with the specified account number');
        return res.redirect('/deposit');
      }

      db.Customer
      .findById(account.customerId)
      .then((customer) => {
        logger.info('Account ID', account.id);
        db.Transaction
        .create({
          type: 'deposit',
          amount: Number(amount),
          previousBalance: Number(account.balance),
          currentBalance: Number(account.balance) + Number(amount),
          issuer: issuer,
          accountId: account.id,
          userId: userId
        })
        .then(function (trx) {
          account
          .update({ balance: trx.currentBalance, recentTransactionId: trx.id, Transaction: [ trx] }, { include: [db.Transaction] })
          .then((newAccount) => {
              req.flash('info', `Account ${account.number} has been credited with GH Cedis ${amount} by ${issuer}`);
              logger.info(`Customer ${customer.firstname} ${customer.lastname} has been credited`);
              res.redirect('/deposit');

              utils.sendMessage(customer.firstPhoneNumber, `Your account ${newAccount.number} has been debited with GH Cedis ${amount} by ${issuer}. New balance is ${newAccount.balance}`, (err, res) => {
                if (err) return logger.error(err);
                logger.info(res);
              });

          }).catch(function (err) { next(err); })
        }).catch(function (err) { next(err); })
      });
    })
    .catch(function (err) {
      return next(err);
    });
  });

  webRouter.get('/withdrawal', function (req, res) {
    if (!req.user) return res.redirect('/');

    var flashErrorMessage = req.flash('error');
    var flashInfoMessage = req.flash('info');

    if (flashErrorMessage.length || flashInfoMessage.length) {
      res.locals.flash = {
        type: flashInfoMessage.length ? 'info' : 'danger',
        message: flashInfoMessage.length ? flashInfoMessage[0] : flashErrorMessage[0]
      };
    }

    res.render('withdrawal', {
      title: 'New Withdrawal'
    })
  });

  webRouter.post('/withdrawal', function (req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var { amount, accountName, accountNumber, issuer } = req.body;
    var { user: { id: userId } } = req;
    db.Account
    .find({ where: { number: accountNumber } })
    .then(function (account) {

      if (!account) {
        req.flash('error', 'No account found with the specified account number');
        return res.redirect('/withdrawal');
      }

      if (account.balance < amount) {
        req.flash('error', 'Amount to be withdrawn is greater than current account balance');
        return res.redirect('/withdrawal');
      }

      db.Customer
      .findById(account.customerId)
      .then((customer) => {
        db.Transaction
        .create({
          type: 'withdrawal',
          amount: Number(amount),
          previousBalance: Number(account.balance),
          currentBalance: Number(account.balance) - Number(amount),
          issuer: issuer,
          accountId: account.id,
          userId: userId
        })
        .then(function (trx) {
          account
          .update({ balance: trx.currentBalance, recentTransactionId: trx.id, Transaction: [ trx] }, { include: [db.Transaction] })
          .then((newAccount) => {
            req.flash('info', `Account ${account.number} has been debited with GH Cedis ${amount} by ${issuer}`);
            res.redirect('/withdrawal');
            utils.sendMessage(customer.firstPhoneNumber, `Your account ${newAccount.number} has been debited with GH Cedis ${amount} by ${issuer}. New balance is ${newAccount.balance}`, (err, res) => {
              if (err) return logger.error(err);
              logger.info(res);
            });
          }).catch(function (err) { next(err); })
        }).catch(function (err) { next(err); })
      });
    })
    .catch(function (err) {
      return next(err);
    });
  });
  return webRouter;
};

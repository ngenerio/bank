var webRouter = require('./web');
var apiRouter = require('./api');

module.exports = {
  webRouter: webRouter,
  apiRouter: apiRouter
};

var Sequelize = require('sequelize');
var path = require('path');
var appLogger = require('./logger');

var sequelize = new Sequelize('dbank', null, null, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
  storage: process.env.DB_PATH
});


var User = sequelize.import(path.join(__dirname, 'models/user'));
var Customer = sequelize.import(path.join(__dirname, 'models/customer'));
var Account = sequelize.import(path.join(__dirname, 'models/account'));
var Transaction = sequelize.import(path.join(__dirname, 'models/transaction'));

Customer.hasOne(Account);
Account.hasMany(Transaction);
Transaction.belongsTo(User);

sequelize.User = User;
sequelize.Customer = Customer;
sequelize.Account = Account;
sequelize.Transaction = Transaction;

appLogger.info('Establishing connection to sqlite db');
sequelize
  .sync()
  .then(function () {
    appLogger.info('Application server database synced fully');
  })
  .catch(function (err) {
    appLogger.error('Error syncing application server database.', err)
  });

module.exports = sequelize;

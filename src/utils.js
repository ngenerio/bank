var bcrypt = require('bcrypt-nodejs');
var SMSGH = require('smsghjs');
var logger = require('./logger');

logger.info(process.env.SMS_CLIENT_ID, process.env.SMS_CLIENT_SECRET, process.env.BANK_NAME);
var smsInstance = new SMSGH({
  clientId: process.env.SMS_CLIENT_ID,
  clientSecret: process.env.SMS_CLIENT_SECRET,
});
smsInstance.setContextPath('v3');

var Message = SMSGH.Message;

var utils = {};

utils.comparePassword = function (password, hashPassword, cb) {
  bcrypt.compare(password, hashPassword, cb)
};

utils.encryptPassword = function (password, cb) {
  bcrypt.hash(password, null, null, cb);
};

utils.sendMessage = function (telephone, message, cb) {
  if (telephone[0] === '0') telephone = `233${telephone.substring(1)}`;
  logger.info(`Sending message to ${telephone}`);
  logger.info(message);
  var msg = new Message({ from: process.env.BANK_NAME, to: telephone, content: message });
  smsInstance.messaging.send(msg, function (err, res) {
    console.log(err, res.body);
  });
};

module.exports = utils;

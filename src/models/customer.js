module.exports = function (sequelize, DataTypes) {
  return sequelize.define('customer', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      defaultValue: 'Mr.'
    },
    lastname: DataTypes.STRING,
    firstname: DataTypes.STRING,
    middlename: DataTypes.STRING,
    formername: DataTypes.STRING,
    maritalStatus: {
      type: DataTypes.ENUM,
      defaultValue: 'single',
      values: ['single', 'married', 'other']
    },
    gender: {
      type: DataTypes.STRING,
      defaultValue: 'Male'
    },
    groupName: DataTypes.STRING,
    isGroup: DataTypes.BOOLEAN,
    dateOfBirth: DataTypes.DATE,
    placeOfBirth: DataTypes.STRING,
    motherMaidenName: DataTypes.STRING,
    nationality: DataTypes.STRING,
    residentPermitNo: DataTypes.STRING,
    permitIssueDate: DataTypes.DATE,
    permitExpiryDate: DataTypes.DATE,
    taxIdentificationNumber: DataTypes.STRING,
    region: DataTypes.STRING,
    residentialAddress: DataTypes.STRING,
    placeofResidence: DataTypes.STRING,
    nearestLandmark: DataTypes.STRING,
    proofOfAddressType: DataTypes.STRING,
    proofOfAddressSerialNumber: DataTypes.STRING,
    mmda: DataTypes.STRING,
    mailingAddress: DataTypes.STRING,
    firstPhoneNumber: DataTypes.STRING,
    secondPhoneNumber: DataTypes.STRING,
    emailAddress: DataTypes.STRING,
    identificationType: {
      type: DataTypes.ENUM,
      values: ['passport', 'national', 'voters', 'drivers']
    },
    identificationNumber: DataTypes.STRING,
    identificationIssueDate: DataTypes.DATE,
    identificationExpiryDate: DataTypes.DATE,
    employmentStatus: {
      type: DataTypes.ENUM,
      values: ['employed', 'self-employed', 'retired', 'unemployed', 'student', 'others']
    },
    dateofEmployment: {
      type: DataTypes.DATE,
      allowNull: true
    },
    annualSalary: {
      type: DataTypes.ENUM,
      values: ['5000', '5001-10000', '10001-20000', '20000']
    },
    employerName: DataTypes.STRING,
    employerAddress: DataTypes.STRING,
    employerNearestLandmark: DataTypes.STRING,
    employerLocation: DataTypes.STRING,
    employerRegion: DataTypes.STRING,
    employmentNature: DataTypes.STRING,
    employerPhoneNumber: DataTypes.STRING,
    employerEmailAddress: DataTypes.STRING,
    employerMobileNumber: DataTypes.STRING,
    nextOfKinTitle: DataTypes.STRING,
    nextOfKinGender: {
      type: DataTypes.ENUM,
      defaultValue: 'male',
      values: ['male', 'female']
    },
    nextOfKinDateOfBirth: DataTypes.DATE,
    nextOfKinFirstName: DataTypes.STRING,
    nextOfKinMiddleName: DataTypes.STRING,
    nextOfKinLastName: DataTypes.STRING,
    nextOfKinRelationship: DataTypes.STRING,
    nextOfKinRegion: DataTypes.STRING,
    nextOfKinResidentialAddress: DataTypes.STRING,
    nextOfKinFirstPhoneNumber: DataTypes.STRING,
    nextOfKinSecondPhoneNumber: DataTypes.STRING,
    beneficialName: DataTypes.STRING,
    spouseName: DataTypes.STRING,
    spouseDateOfBirth: DataTypes.STRING,
    spouseOccupation: DataTypes.STRING,
    levelOfDeposits: DataTypes.STRING,
    frequencyOfDeposits: DataTypes.STRING,
    expectAnnualIncomeFromOtherSources: DataTypes.STRING,
    businessType: DataTypes.STRING,
    businessAddress: DataTypes.STRING,
    cardPreference: {
      type: DataTypes.ENUM,
      values: ['atm', 'ghlink', 'others']
    },
    internetBanking: DataTypes.BOOLEAN,
    mobileBanking: DataTypes.BOOLEAN,
    transactionAlertEmail: DataTypes.BOOLEAN,
    transactionAlertSMS: DataTypes.BOOLEAN,
    transactionAlertPreference: {
      type: DataTypes.ENUM,
      defaultValue: 'sms',
      values: ['sms', 'email']
    },
    statementFrequency: {
      type: DataTypes.ENUM,
      values: ['semi-annually', 'annually']
    },
  });
}

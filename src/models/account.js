module.exports = function (sequelize, DataTypes) {
  return sequelize.define('account', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    type: {
      type: DataTypes.ENUM,
      values: ['individual', 'group', 'normal']
    },
    branch: DataTypes.STRING,
    number: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      unique: true
    },
    balance: DataTypes.FLOAT,
    recentTransactionId: DataTypes.UUID
  });
};

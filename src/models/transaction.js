module.exports = function (sequelize, DataTypes) {
  return sequelize.define('transaction', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: true
    },
    issuer: DataTypes.STRING,
    type: {
      type: DataTypes.ENUM,
      defaultValue: 'deposit',
      values: ['deposit', 'withdrawal', 'loan']
    },
    amount: {
      type: DataTypes.FLOAT,
      defaultValue: 0.00
    },
    previousBalance: {
      type: DataTypes.FLOAT,
      defaultValue: 0.00
    },
    currentBalance: {
      type: DataTypes.FLOAT,
      defaultValue: 0.00
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }
  });
};

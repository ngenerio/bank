module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user', {
    name: DataTypes.STRING,
    telephone: DataTypes.STRING,
    position: DataTypes.STRING,
    title: {
      type: DataTypes.STRING,
      defaultValue: 'Mr.'
    },
    gender: {
      type: DataTypes.ENUM,
      values: ['male', 'female']
    },
    username: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    picture: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    state: {
      type: DataTypes.ENUM,
      defaultValue: 'inactive',
      values: ['active', 'inactive']
    }
  }, {
    indexes: [
      { unique: true, fields: ['email'] }
    ]
  });
};

module.exports = function (hbs) {
  hbs.registerHelper('trx', function (transactions) {
    var string = '';

    if (!transactions.length) {
      return null;
    }

    for (var i = 0; i < transactions.length; i++) {
      const transaction = transactions[i];

      if (i === 0 || i%4 === 0) string += '<div class="row">';

      string += `
        <div class="col-xs-3 col-md-3 transaction-card">
          <h4>Transaction Type: ${transaction.type}</h4>
          <p>Amount ${transaction.type === 'deposit' ? 'deposited' : 'withdrawn'}: <strong>GH₵${transaction.amount}</strong></p>
          <p>Initiated by ${transaction.user.name}</p>
          <p>
            <a href="/admin/transaction/${transaction.id}" class="btn btn-primary">View Details</a>
          </p>
        </div>
      `;

      if ((i+1)%4 === 0) string += '</div>'
    }

    return new hbs.SafeString(string);
  });

  hbs.registerHelper('ifvalue', function (conditional, options) {
    if (options.hash.value === conditional) {
      return options.fn(this)
    } else {
      return options.inverse(this);
    }
  });
};

var express = require('express');
var session = require('express-session');
var flash = require('express-flash');
var bodyParser = require('body-parser');
var logger = require('morgan');
var compression = require('compression');
var sass = require('node-sass-middleware');
var cookieParser = require('cookie-parser');
var SQLiteStore = require('connect-sqlite3')(session);
var sequelize = require('sequelize');
var hbs = require('express-hbs');
var path = require('path');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var appLogger = require('./logger');
var routes = require('./routes');
var utils = require('./utils');
var db = require('./db');
var viewHelpers = require('./helpers');

var app = express();

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  appLogger.debug('deserializeUser : ', id);
  db.User
  .findOne({ where: { id } })
  .then(function(user) {
    done(null, user);
  })
  .catch(function (err) {
    done(err, null);
  });
});

passport.use(new LocalStrategy({ usernameField: 'email' }, function (email, password, done) {
  db.User.findOne({ where: { email: email.toLowerCase() } })
  .then(function (user) {
    if (!user) {
      return done(null, false, { message: `Email ${email} not found.` });
    }

    appLogger.info(password, user.password);
    utils.comparePassword(password, user.password, function (err, isMatch) {
      appLogger.info('User password compare: ', isMatch);

      if (isMatch) {
        return done(null, user);
      }
      return done(null, false, { message: 'Invalid email or password.' });
    });
  })
  .catch((err) => {
    done(err);
  });
}));

app.locals.name = process.env.BANK_NAME;

if (process.env.NODE_ENV === 'development') {
  app.set('port', process.env.SERVER_PORT);
} else {
  app.set('port', process.env.PORT);
}

viewHelpers(hbs);

app.engine('hbs', hbs.express4({ partialsDir: path.join(__dirname, '../views/partials') }))
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '../views'));
app.use(compression());
app.use(sass({
  src: path.join(__dirname, '../public'),
  dest: path.join(__dirname, '../public'),
  outputStyle: 'compressed'
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  store: new SQLiteStore({
    table: 'sessions',
    db: 'sessions',
    dir: path.join(__dirname, '../data')
  }),
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: process.env.SESSION_AGE * 24 * 60 * 60 * 1000 }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static(path.join(__dirname, '../public')));


app.use(routes.webRouter(db));
app.use(routes.apiRouter(db));

app.listen(app.get('port'), function (err) {
  if (err) {
    return appLogger.error('Error starting application server on port: ' + app.get('port'), err);
  }

  appLogger.info('Application server started on port: ' + app.get('port'));
})
